﻿using UnityEngine;


public enum BubbleType
{
  ShooterBubble = 0,
  StaticBubble = 1
}

public class BubblePool : ObjectPool
{
  private GameObject bubbleGridObject;
  
  public BubblePool(GameObject _prefab, int _size, GameObject _bubbleGridObject) : base(_prefab, _size)
  {
    bubbleGridObject = _bubbleGridObject;
  }

  public Bubble Get(Vector3 pos, BubbleType bubbleType)
  {
    GameObject gameObject = GetObject(pos, Quaternion.identity);
    gameObject.transform.SetParent(bubbleGridObject.transform);
    if (bubbleType == BubbleType.ShooterBubble)
      gameObject.layer = LayerMask.NameToLayer("BubbleShooter");
    else if (bubbleType == BubbleType.StaticBubble)
      gameObject.layer = LayerMask.NameToLayer("BubbleStatic");
    else
      Debug.LogWarning("Got new object with undefined bubble type from pool.");
    return gameObject.GetComponent<Bubble>();
  }

  public void Free(Bubble bubble)
  {
    FreeObject(bubble.gameObj);
    bubble.gameObj.transform.SetParent(null);
  }
}