﻿using UnityEngine;

public delegate float EasingFunction(float x);
public class Easing
{
  public static float Linear(float x)
  {
    return x;
  }

  public static float InSine(float x)
  {
    return 1 - Mathf.Cos((x - Mathf.PI) * 0.5f);
  }

  public static float OutSine(float x)
  {
    return Mathf.Sin((x - Mathf.PI) * 0.5f);
  }

  public static float InOutSine(float x)
  {
    return -(Mathf.Cos(Mathf.PI * x) - 1.0f) * 0.5f;
  }

  public static float InQuad(float x)
  {
    return x * x;
  }

  public static float OutQuad(float x)
  {
    return 1 - (1 - x) * (1 - x);
  }

  public static float InOutQuad(float x)
  {
    return x < 0.5f ? 2.0f * x * x : 1.0f - Mathf.Pow(-2.0f * x + 2.0f, 2.0f) * 0.5f;
  }

  public static float InCubic(float x)
  {
    return x * x * x;
  }

  public static float OutCubic(float x)
  {
    return 1.0f - Mathf.Pow(1.0f - x, 3.0f);
  }

  public static float InOutCubic(float x)
  {
    return x < 0.5f ? 4.0f * x * x * x : 1.0f - Mathf.Pow(-2.0f * x + 2.0f, 3.0f) * 0.5f;
  }

  public static float InQuart(float x)
  {
    return x * x * x * x;
  }

  public static float OutQuart(float x)
  {
    return 1.0f - Mathf.Pow(1.0f - x, 4.0f);
  }

  public static float InOutQuart(float x)
  {
    return x < 0.5f ? 8.0f * x * x * x * x : 1.0f - Mathf.Pow(-2.0f * x + 2.0f, 4.0f) * 0.5f;
  }

  public static float InQuint(float x)
  {
    return x * x * x * x * x;
  }

  public static float OutQuint(float x)
  {
    return 1.0f - Mathf.Pow(1.0f - x, 5.0f);
  }

  public static float InOutQuint(float x)
  {
    return x < 0.5f ? 16.0f * x * x * x * x * x : 1.0f - Mathf.Pow(-2.0f * x + 2.0f, 5.0f) * 0.5f;
  }

  public static float InExpo(float x)
  {
    return Mathf.Approximately(x, 0.0f) ? 0.0f : Mathf.Pow(2.0f, 10.0f * x - 10.0f);
  }

  public static float OutExpo(float x)
  {
    return Mathf.Approximately(x, 1.0f) ? 1.0f : 1.0f - Mathf.Pow(2.0f, -10.0f * x);
  }

  public static float InOutExpo(float x)
  {
    return Mathf.Approximately(x, 0.0f)
      ? 0.0f
      : Mathf.Approximately(x, 1.0f)
        ? 1.0f
        : x < 0.5f 
          ? Mathf.Pow(2.0f, 20.0f * x - 10.0f) * 0.5f
          : (2.0f - Mathf.Pow(2.0f, -20.0f * x + 10.0f)) * 0.5f;
  }

  public static float InCirc(float x)
  {
    return 1.0f - Mathf.Sqrt(1.0f - Mathf.Pow(x, 2.0f));
  }

  public static float OutCirc(float x)
  {
    return Mathf.Sqrt(1.0f - Mathf.Pow(x - 1.0f, 2.0f));
  }

  public static float InOutCirc(float x)
  {
    return x < 0.5f
      ? (1.0f - Mathf.Sqrt(1.0f - Mathf.Pow(2.0f * x, 2.0f))) * 0.5f
      : (Mathf.Sqrt(1.0f - Mathf.Pow(-2.0f * x + 2.0f, 2.0f)) + 1.0f) * 0.5f;
  }

  private const float c1 = 1.70158f;
  private const float c3 = 2.70158f;
  public static float InBack(float x)
  {
    return c1 * x * x * x - c3 * x * x;
  }

  public static float OutBack(float x)
  {
    return 1.0f + c3 + Mathf.Pow(x - 1.0f, 3.0f) + c1 * Mathf.Pow(x - 1.0f, 2.0f);
  }

  private const float c2 = c1 * 1.525f;
  public static float InOutBack(float x)
  {
    return x < 0.5f
      ? (Mathf.Pow(2.0f * x, 2.0f) * ((c2 + 1.0f) * 2.0f * x - c2)) * 0.5f
      : (Mathf.Pow(2.0f * x - 2.0f, 2.0f) * ((c2 + 1.0f) * (x * 2.0f - 2.0f) + c2) + 2.0f) * 0.5f;
  }

  private const float c4 = (2.0f * Mathf.PI) / 3.0f;
  public static float InElastic(float x)
  {
    return Mathf.Approximately(x, 0.0f)
      ? 0.0f
      : Mathf.Approximately(x, 1.0f)
        ? 1.0f
        : -Mathf.Pow(2.0f, 10.0f * x - 10.0f) * Mathf.Sin((x * 10.0f - 10.75f) * c4);
  }

  public static float OutElastic(float x)
  {
    return Mathf.Approximately(x, 0.0f)
      ? 0.0f
      : Mathf.Approximately(x, 1.0f)
        ? 1.0f
        : Mathf.Pow(2.0f, -10.0f * x) * Mathf.Sin((x * 10.0f - 0.75f) * c4) + 1.0f;
  }

  private const float c5 = (2.0f * Mathf.PI) / 4.5f;
  public static float InOutElastic(float x)
  {
    return Mathf.Approximately(x, 0.0f)
      ? 0.0f
      : Mathf.Approximately(x, 1.0f)
        ? 1.0f
        : x < 0.5f
          ? -(Mathf.Pow(2.0f, 20.0f * x - 10.0f) * Mathf.Sin((20.0f * x - 11.125f) * c5)) * 0.5f
          : (Mathf.Pow(2.0f, -20.0f * x + 10.0f) * Mathf.Sin((20.0f * x - 11.125f) * c5)) * 0.5f + 1.0f;
  }

  public static float InBounce(float x)
  {
    return 1.0f - OutBounce(1.0f - x);
  }

  private const float n1 = 7.5625f;
  private const float d1 = 2.75f;
  public static float OutBounce(float x)
  {
    if (x < 1.0f / d1)
      return n1 * x * x;
    else if (x < 2.0f / d1)
      return n1 * (x -= 1.5f / d1) * x + 0.75f;
    else if (x < 2.5f / d1)
      return n1 * (x -= 2.25f / d1) * x + 0.9375f;
    else
      return n1 * (x -= 2.625f / d1) * x + 0.984375f;
  }

  public static float InOutBounce(float x)
  {
    return x < 0.5f
      ? (1.0f - OutBounce(1.0f - 2.0f * x)) * 0.5f
      : (1.0f + OutBounce(2.0f * x - 1.0f)) * 0.5f;
  }
}