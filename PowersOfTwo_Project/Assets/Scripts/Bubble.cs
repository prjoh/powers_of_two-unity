﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using Vector3 = UnityEngine.Vector3;


public class Bubble : MonoBehaviour
{
  public GameObject gameObj;
  public MeshRenderer meshRenderer;
  public Rigidbody2D rb2D;

  // Reference to manager for cleanup
  public GridManager manager;
  // Bubble Grid Info
  public Vector2Int gridIndex;
  public bool connected;

  // value is equal to the power value of bubble (e.g. 2 -> 1, 4 -> 2, etc.)
  public int value;

  // Bubble movement
  private bool _moving;
  private Vector3 _from, _to;
  private EasingFunction _easeFunction;
  private float _duration, _elapsed;

  // Falling bubble state
  private bool _falling;

  private void Awake()
  {
    manager = GameObject.Find("BubbleGrid").GetComponent<GridManager>();
  }

  private void FixedUpdate()
  {
    if (_falling)
    {
      rb2D.position = rb2D.position + rb2D.velocity * Time.deltaTime;
      rb2D.velocity = rb2D.velocity + Physics2D.gravity * Time.deltaTime; 
    }
  }

  public void Disconnect(Vector2 velocity)
  {
    rb2D.velocity = velocity;
    _falling = true;
  }
  
  public IEnumerator MoveAlongPath(Vector3[] path, float speed, EasingFunction easeFunction, UnityAction callback)
  {
    foreach (var position in path)
    {
      StartCoroutine(MoveTo(position, speed, easeFunction, () => {}));
      while (_moving) yield return null;
    }

    callback();
  }

  public IEnumerator MoveTo(Vector3 position, float speed, EasingFunction easeFunction, UnityAction callback)
  {
#if UNITY_EDITOR
    Assert.IsTrue(_moving == false);
#endif
    _from = gameObj.transform.position;
    _to = position;
    _duration = Vector3.Distance(_from, _to) / speed;
    _easeFunction = easeFunction;
    _elapsed = 0.0f;
    _moving = true;

    while (_moving)
    {
      _elapsed += Time.deltaTime;
      gameObj.transform.position = Vector3.Lerp(_from, _to, _easeFunction(_elapsed / _duration));

      if (_elapsed < _duration)
        yield return null;
      else
      {
        gameObj.transform.position = _to;
        _moving = false;
      }
    }

    callback();
  }
  
  // called when this Bubble collides with GameObject.
  void OnTriggerEnter2D(Collider2D col)
  {
    Debug.Log("Bubble collided with " + col.name);

    if (col.gameObject.CompareTag("WallCollider2D"))
    {
      var collisionPoint = col.ClosestPoint(rb2D.position);
      var collisionNormal = (collisionPoint - rb2D.position).normalized;
      Debug.DrawRay(collisionPoint, -rb2D.velocity, Color.blue, 10.0f);
      rb2D.velocity = Vector2.Reflect(rb2D.velocity, collisionNormal);
      Debug.DrawLine(rb2D.position, collisionPoint, Color.magenta, 10.0f);
      Debug.DrawRay(collisionPoint, rb2D.velocity, Color.green, 10.0f);      
    }
    else if (col.gameObject.CompareTag("FloorCollider2D"))
    {
      rb2D.velocity = Vector2.zero;
      _falling = false;
      manager.DestroyBubble(this);
    }
  }
}
