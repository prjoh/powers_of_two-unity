﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;


public class GridManager : MonoBehaviour
{
  public GameObject bubblePrefab;
  public BubbleData[] bubbleData;
  public GameObject gridGameObject;
  
  private const int gridSizeX = 6;
  private const int gridSizeY = 14;
  
  private float _bubbleDiameter;
  private Vector3 topLeftSpawn;
  private float offsetX;
  private float offsetY;
  private bool oddRow = true;
  private bool gridOverflow = false;
  
  private int bubblePoolSize;
  private BubblePool bubblePool;
  private Bubble[][] bubbleGrid;
  private float[] cdf;
  
  public Vector3 BubbleShiftOffset => offsetY * Vector3.down;
  public bool GridOverflow => gridOverflow;
  public float BubbleDiameter => _bubbleDiameter;

#if UNITY_EDITOR
  public Button moveBackButton;
  public Button moveForwardButton;
  
  private List<Tuple<int[,], bool>> gridHistory = new List<Tuple<int[,], bool>>();
  private int time = 0;

  private void UpdateGridHistory()
  {
    while (gridHistory.Count > time)
      gridHistory.RemoveAt(gridHistory.Count - 1);
    
    int[,] grid = new int[gridSizeY, gridSizeX];
    for (int y = 0; y < gridSizeY; y++)
    {
      for (int x = 0; x < gridSizeX; x++)
      {
        if (!bubbleGrid[y][x])
          grid[y,x] = 0;
        else
          grid[y,x] = bubbleGrid[y][x].value;
      }
    }
    gridHistory.Add(new Tuple<int[,], bool>(grid, oddRow));
    UpdateTime(1);
  }

  private void UpdateTime(int timeDirection)
  {
    time += timeDirection;
    if (time < 2)
      moveBackButton.interactable = false;
    else
      moveBackButton.interactable = true;
    if (time == gridHistory.Count)
      moveForwardButton.interactable = false;
    else
      moveForwardButton.interactable = true;
  }
  
  public void RestoreGrid(int timeDirection)
  {
    UpdateTime(timeDirection);
    
    DestroyGrid();

    Tuple<int[,], bool> gridState = gridHistory[time-1];
    int[,] grid = gridState.Item1;
    oddRow = gridState.Item2;
    for (int y = 0; y < gridSizeY; y++)
    {
      for (int x = 0; x < gridSizeX; x++)
      {
        if (grid[y,x] == 0)
          bubbleGrid[y][x] = null;
        else
        {
          Bubble bubble = bubblePool.Get(GridToWorld(x, y), BubbleType.StaticBubble);
          bubble.gridIndex.Set(x, y);
          bubble.connected = true;
          bubble.value = grid[y,x];
          BubbleData bd = bubbleData[grid[y,x] - 1];
          bubble.meshRenderer.material = bd.material;
          bubble.name = "Bubble_" + x + "_" + y;
          bubbleGrid[y][x] = bubble;
        }
      }
    }
  }
#endif

  private void Awake()
  {
    _bubbleDiameter = bubblePrefab.transform.localScale.x;
    topLeftSpawn = gridGameObject.transform.position;
    offsetX = _bubbleDiameter;
    offsetY = Mathf.Sqrt((_bubbleDiameter * _bubbleDiameter) - ((_bubbleDiameter * _bubbleDiameter * 0.25f)));
    
    bubblePoolSize = (gridSizeX * gridSizeY) + 1;
    bubblePool = new BubblePool(bubblePrefab, bubblePoolSize, gridGameObject);
    
    // Build cumulative distribution function (CDF)
    int size = bubbleData.Length - 1; // No weight for 2048
    cdf = new float[size];
    for (int i = 0; i < size; i++)
    {
      float accumWeight = 0.0f;
      for (int j = 0; j <= i; j++) accumWeight += bubbleData[j].weight;
      cdf[i] = accumWeight;
    }
    
    // Allocate Bubble Grid
    bubbleGrid = new Bubble[gridSizeY][];
    for (int y = 0; y < gridSizeY; y++)
      bubbleGrid[y] = new Bubble[gridSizeX];
  }

  public Bubble SpawnShooter(Vector3 position)
  {
    Bubble bubble = bubblePool.Get(position, BubbleType.ShooterBubble);
    bubble.gridIndex.Set(-1, -1);
    bubble.connected = false;
    BubbleData bd = GetRandomBubbleData();
    bubble.value = bd.value;
    bubble.meshRenderer.material = bd.material;
    return bubble;
  }

  public void CreateGrid(int startSize)
  {
#if UNITY_EDITOR
    gridHistory.Clear();
    time = 0;
#endif
    gridOverflow = false;
    for (int y = 0; y < gridSizeY; y++)
    {
      if (y == startSize) break;
      for (int x = 0; x < gridSizeX; x++)
      {
        Bubble bubble = bubblePool.Get(GridToWorld(x, y), BubbleType.StaticBubble);
        bubble.gridIndex.Set(x, y);
        bubble.connected = true;
        BubbleData bd = GetRandomBubbleData();
        bubble.value = bd.value;
        bubble.meshRenderer.material = bd.material;
        bubbleGrid[y][x] = bubble;
#if UNITY_EDITOR
        bubble.name = "Bubble_" + x + "_" + y;
#endif
      }
    }
#if UNITY_EDITOR
    UpdateGridHistory();
#endif
  }
  
#if UNITY_EDITOR
  public TextAsset gridLayout;
  public void CreateGridFromFile()
  {
    string[] stringSeparators = new string[] {"\r\n", "\n"};
    string[] rows = gridLayout.text.Split(stringSeparators, StringSplitOptions.None);

    int[,] ids = new int[gridSizeX, rows.Length];
    for (int i = 0; i < rows.Length; i++)
    {
      String[] lineData = rows[i].Split(',');
      for (int j = 0; j < lineData.Length; j++)
      {
        int id = -1;
        int.TryParse(lineData[j], out id);
        ids[j, i] = id;
      }
    }

    gridHistory.Clear();
    time = 0;
    gridOverflow = false;
    for (int y = 0; y < rows.Length; y++)
    {
      for (int x = 0; x < gridSizeX; x++)
      {
        if (ids[x, y] == -1)
          continue;
        Bubble bubble = bubblePool.Get(GridToWorld(x, y), BubbleType.StaticBubble);
        bubble.gridIndex.Set(x, y);
        bubble.connected = true;
        BubbleData bd = bubbleData[ids[x, y]];
        bubble.value = bd.value;
        bubble.meshRenderer.material = bd.material;
        bubbleGrid[y][x] = bubble;
        bubble.name = "Bubble_" + x + "_" + y;
      }
    }
  }
#endif  

  public void DestroyGrid()
  {
    for (int y = 0; y < gridSizeY; y++)
    {
      for (int x = 0; x < gridSizeX; x++)
      {
        if (bubbleGrid[y][x] == null)
          continue;
#if UNITY_EDITOR
        bubbleGrid[y][x].name = "BubblePrefab(Clone)";
#endif
        bubblePool.Free(bubbleGrid[y][x]);
        bubbleGrid[y][x] = null;
      }
    }
  }

  public void DestroyBubble(Bubble bubble)
  {
#if UNITY_EDITOR
    bubble.name = "BubblePrefab(Clone)";
#endif
    bubblePool.Free(bubble);
    
    int x = bubble.gridIndex.x;
    int y = bubble.gridIndex.y;
    if (x != -1 && y != -1)
      bubbleGrid[y][x] = null;
  }

  private void InsertBubble(Bubble bubble, Vector2Int index)
  {
#if UNITY_EDITOR
    bubble.name = "Bubble_" + index.x + "_" + index.y;
#endif
    bubble.gridIndex.Set(index.x, index.y);
    bubble.connected = true;
    bubbleGrid[index.y][index.x] = bubble;
  }

  private BubbleData GetRandomBubbleData()
  {
    float rand = Random.Range(0.0f, 1.0f);
    for (int i = 0; i < cdf.Length; i++)
      if (cdf[i] >= rand) return bubbleData[i];
    return null;
  }
  
  public Vector3 GridToWorld(int x, int y)
  {
    Vector3 pos = topLeftSpawn + x * offsetX * Vector3.right - y * offsetY * Vector3.up;
    if ((y % 2 == 1 && oddRow) || (y % 2 == 0 && !oddRow))
      pos += 0.5f * offsetX * Vector3.right;
    
    return pos;
  }
  
  public void GetEmptyNeighbour (GameObject bubbleObject, Vector3 hitPosition, out Vector2Int emptyIndex, out Vector3 emptyPosition)
  {
    emptyIndex = new Vector2Int(-1, -1);
    emptyPosition = Vector3.negativeInfinity;

    Bubble bubble = bubbleObject.GetComponent<Bubble>();
    float minDist = Mathf.Infinity;

    foreach (var index in GetNeighbourIndexes(bubble.gridIndex, true))
    {
      Vector3 pos = GridToWorld(index.x, index.y);
      float dist = (pos - hitPosition).sqrMagnitude;
      if (dist < minDist)
      {
        emptyPosition = pos;
        emptyIndex = index;
        minDist = dist;
      }
    }

#if UNITY_EDITOR
    Assert.IsTrue(emptyPosition != Vector3.negativeInfinity && emptyIndex.x != -1 && emptyIndex.y != -1,
                  "Illegal result in GetEmptyNeighbour: No neighbours were found!");
#endif
  }

  private readonly int[][][] _oddRDirections = new int[2][][] 
  { new int[6][]  { 
      new int[2] { +1, 0 }, new int[2] { 0, -1 }, new int[2] { -1, -1 }, 
      new int[2] { -1, 0 }, new int[2] { -1, +1 }, new int[2] { 0, +1 }, },
    new int[6][] { 
      new int[2] { +1,  0 }, new int[2] { +1, -1 }, new int[2] { 0, -1 },
      new int[2] { -1,  0 }, new int[2] { 0, +1 }, new int[2] { +1, +1 }, }, };
  private List<Vector2Int> GetNeighbourIndexes(Vector2Int gridIndex, bool isEmpty)
  {
    List<Vector2Int> indexes = new List<Vector2Int>();
    int parity;
    if (oddRow)
      parity = gridIndex.y % 2;
    else
      parity = (gridIndex.y + 1) % 2;
    for (int i = 0; i < 6; i++)
    {
      int x = gridIndex.x + _oddRDirections[parity][i][0];
      int y = gridIndex.y + _oddRDirections[parity][i][1];
      if (x < 0 || x >= gridSizeX || y < 0 || y >= gridSizeY)
        continue;
      if (!bubbleGrid[y][x] == isEmpty)
        indexes.Add(new Vector2Int(x, y));
    }
    return indexes;
  }

  /*
   * mergeLocations:       holds all positions of merge goal locations, after shooter bubble is inserted into grid
   * mergeBubble:          holds lists of bubble objects that are part of a merge; the goal bubbles of the merges are placed at the end of each list
   * disconnectedBubbles:  holds lists of bubble objects that are disconnected due to a merge operation
   * explodingBubbles:     holds all bubble objects that explode due to an 11th power explosion
   * shiftBubbles:         holds all bubble objects that need to be shifted after new Bubble row was spawned
   */
  public void MergeBubbles (Bubble shooterBubble, 
    ref List<Vector3> mergeLocations, 
    ref List<List<Bubble>> mergeBubbles,
    ref List<List<Bubble>> disconnectedBubbles,
    ref List<Bubble> explodingBubbles,
    ref List<Bubble> shiftBubbles)
  {
    // Insert shooter bubble into the grid
    InsertBubble(shooterBubble, shooterBubble.gridIndex);
#if UNITY_EDITOR
    UpdateGridHistory();
#endif

    bool hasMerged = false;
    Vector2Int newBubbleIndex = shooterBubble.gridIndex;
    while (true)
    {
      // Creates a list of indexes of bubbles with same value (inclusive newBubbleIndex)
      HashSet<Vector2Int> merge = GetMerge(newBubbleIndex);
      if (merge.Count == 0)
        break;

      hasMerged = true;
      // new bubble will hold value of starting bubble + merges (we should decrement 1 because merge also holds initial bubble index)
      int newBubbleValue = bubbleGrid[newBubbleIndex.y][newBubbleIndex.x].value + merge.Count - 1;
      // all bubbles will merge to position, where a new merge is possible
      newBubbleIndex = GetNextInsertIndex(merge, newBubbleValue);
      mergeLocations.Add(GridToWorld(newBubbleIndex.x, newBubbleIndex.y));
      // Update value of the bubble at merge goal position
      bubbleGrid[newBubbleIndex.y][newBubbleIndex.x].value = bubbleData[newBubbleValue - 1].value;

      // Clear all merging bubbles from grid (except merge goal) and keep object references in a list
      List<Bubble> bubbles = new List<Bubble>();
      foreach (var index in merge)
      {
        if (index == newBubbleIndex)
          continue;
        Bubble bubble = bubbleGrid[index.y][index.x];
        bubbleGrid[index.y][index.x] = null;  // TODO: Instead of setting grid to null, set destroyed flag on bubble?
        bubble.gridIndex.Set(-1, -1);
        bubbles.Add(bubble);
      }
#if UNITY_EDITOR
      UpdateGridHistory();
#endif
      bubbles.Add(bubbleGrid[newBubbleIndex.y][newBubbleIndex.x]);
      mergeBubbles.Add(bubbles);
      
      // Explosion
      if (newBubbleValue == 11)
      {
        Bubble expolsionBubble = bubbleGrid[newBubbleIndex.y][newBubbleIndex.x];
        explodingBubbles.Add(expolsionBubble);
        foreach (var index in GetNeighbourIndexes(newBubbleIndex, false))
        {
          if (index.y == 0)
            continue;
          Bubble bubble = bubbleGrid[index.y][index.x];
          explodingBubbles.Add(bubble);
          bubbleGrid[index.y][index.x] = null;  // TODO: Instead of setting grid to null, set destroyed flag on bubble?
          bubble.gridIndex.Set(-1, -1);
        }
        bubbleGrid[newBubbleIndex.y][newBubbleIndex.x] = null;  // TODO: Instead of setting grid to null, set destroyed flag on bubble?
        expolsionBubble.gridIndex.Set(-1, -1);
#if UNITY_EDITOR
        UpdateGridHistory();
#endif
      }
      
      // Check for disconnected Bubbles; we can only set them null after we finished checking for all merges TODO: WHY???????
      // TODO: Because, when we merge into a disconnecetd region, our final goal bubble will eventually become disconnecetd
      // TODO: This is a problem because our breakout condition for THIS while loop is that we check for merges on our final bubble (which should end up as merge.Count == 0)
      // TODO: However, in our case, this final bubble will already be removed due to disconnection, so it results in a null pointer reference
      // TODO: I AM NOT SURE THERE IS A SIMPLE WAY FOR FIXING THIS. What if we omit goal bubbles when checking for disconnection???
      // TODO: BECAUSE we do not know, which bubbles of a disconnected region might be part of a future merge (so which bubbles should be kept?)
      // TODO: This leads me to believe, that we should do just one final check for disconnecetd bubbles, after merging loop is done
      // TODO: DOWNSIDES of this are, that we can only choose one point in time to remove disconnecetd bubbles visually
      // TODO: A more complicated way to handle this would be to check our all discopnnecetd lists for every merge check,
      // TODO: and remove all bubbles from disconnecetd that are found to be merging candidates
      // TODO: This would also result in the necessity to do one final additional disconnecetd check, after we breaked out of the merge while loop
      // TODO: this is not entierely insane, cause the original disconnecetd datastructures are HashSets, so lookup is O(1)
      // TODO: But is it really correct?
      // TODO: IS IT NOT ENOUGH TO JUST IGNORE NEW BUBBLE INDEX IN DISCONNECTION CHECKING?
      List<Bubble> disconnected = GetDisconnected(true, newBubbleIndex);
      disconnectedBubbles.Add(disconnected);
      foreach (var bubble in disconnected)
      {
        bubbleGrid[bubble.gridIndex.y][bubble.gridIndex.x] = null;  // TODO: Instead of setting grid to null, set destroyed flag on bubble?
        bubble.gridIndex.Set(-1, -1);
      }
#if UNITY_EDITOR
      if (disconnected.Count > 0)
        UpdateGridHistory();
#endif
      
      // We break early if the new Bubble reaches max value
      if (newBubbleValue == 11)
        break;
    }
    
    // TODO: THIS HAS TO BE PERFORMED INSIDE WHILE LOOP; now a bubble can be in multiple disconncetd lists
    // TODO: however, the bubbles that are found to be disconnecetd, have to be removed for the next merge test
    // Remove all disconnected Bubble objects from our grid
//     foreach (var disconnected in disconnectedBubbles)
//     {
//       foreach (var bubble in disconnected)
//       {
//         bubbleGrid[bubble.gridIndex.y][bubble.gridIndex.x] = null;
//         bubble.gridIndex.Set(-1, -1);
//       }
// #if UNITY_EDITOR
//       if (disconnected.Count > 0)
//         UpdateGridHistory();
// #endif
//     }

    // We have to do a final check for disconnection here, in case the bubbles merged into a disconnected region
    // We are safe if the final bubble explodes; otherwise we have to check if the final bubble is disconnected
    if (explodingBubbles.Count == 0)
    {
      List<Bubble> disconnected = GetDisconnected(false);
      disconnectedBubbles.Add(disconnected);
      foreach (var bubble in disconnected)
      {
        bubbleGrid[bubble.gridIndex.y][bubble.gridIndex.x] = null;  // TODO: Instead of setting grid to null, set destroyed flag on bubble?
        bubble.gridIndex.Set(-1, -1);
      }
#if UNITY_EDITOR
      if (disconnected.Count > 0)
        UpdateGridHistory();
#endif
    }
      
    // If a merge has occurred, we copy the whole grid one row down
    if (hasMerged)
    {
      if (LastRow() == gridSizeY - 1)
      {
        gridOverflow = true;
        return;
      }

      MoveGrid(ref shiftBubbles);
#if UNITY_EDITOR
      UpdateGridHistory();
#endif
    }
  }

  private HashSet<Vector2Int> GetMerge(Vector2Int newBubbleIndex)
  {
    int mergeValue = bubbleGrid[newBubbleIndex.y][newBubbleIndex.x].value;
    Debug.Log("Find merge at " + newBubbleIndex + " with value " + mergeValue);
    // Maximum matches is equal to maximum power value minus the starting merge value,
    // but we should add 1 since we keep starting bubble index in our merge list
    int maxMatches = 11 - mergeValue + 1;
    HashSet<Vector2Int> visited = new HashSet<Vector2Int> {newBubbleIndex};
    HashSet<Vector2Int> merge = new HashSet<Vector2Int> {newBubbleIndex};
    Queue<Vector2Int> queue = new Queue<Vector2Int>();
    queue.Enqueue(newBubbleIndex);
    
    while (queue.Count > 0)
    {
      Vector2Int next = queue.Dequeue();
      foreach (var neighbourIndex in GetNeighbourIndexes(next, false))
      {
        Debug.Log("Looking at neighbour " + neighbourIndex);
        Bubble neighbour = bubbleGrid[neighbourIndex.y][neighbourIndex.x];
        if (visited.Contains(neighbour.gridIndex))
          continue;
        
        visited.Add(neighbour.gridIndex);
        if (neighbour.value != mergeValue || neighbour.gridIndex.y == 0)
          continue;
        
        Debug.Log("Merge found with " + neighbour.gridIndex);
        merge.Add(neighbour.gridIndex);
        if (merge.Count == maxMatches)
        {
          queue.Clear();
          break;
        }
        queue.Enqueue(neighbour.gridIndex);
      }
    }

    if (merge.Count == 1)
      merge.Clear();
    
    return merge;
  }

  private Vector2Int GetNextInsertIndex(HashSet<Vector2Int> merge, int newValue)
  {
    Vector2Int newBubbleIndex = merge.Last();
    int maxMerges = 0;
    foreach (var index in merge)
    {
      int merges = 0;
      foreach (var n in GetNeighbourIndexes(index, false))
      {
        if (merge.Contains(n))
          continue;
        if (bubbleGrid[n.y][n.x].value == newValue)
          merges += 1;
      }

      if (merges >= maxMerges)
      {
        newBubbleIndex = index;
        maxMerges = merges;
      }
    }
    return newBubbleIndex;
  }
  
  private List<Bubble> GetDisconnected(bool ignoreNewBubble, Vector2Int newBubbleIndex = new Vector2Int())
  {
    HashSet<Bubble> disconnected = new HashSet<Bubble>();
    for (int y = 1; y < gridSizeY; y++)
    {
      for (int x = 0; x < gridSizeX; x++)
      {
        if (!bubbleGrid[y][x] || (ignoreNewBubble && x == newBubbleIndex.x && y == newBubbleIndex.y)) // TODO: Delete this after testing
          continue;
      
        bubbleGrid[y][x].connected = false;
        disconnected.Add(bubbleGrid[y][x]);
      }
    }
    
    HashSet<Vector2Int> visited = new HashSet<Vector2Int>();
    Queue<Vector2Int> queue = new Queue<Vector2Int>();
    queue.Enqueue(bubbleGrid[0][0].gridIndex);
    while (queue.Count > 0 && disconnected.Count > 0)
    {
      Vector2Int nextIndex = queue.Dequeue();
      visited.Add(nextIndex);
      Bubble nextBubble = bubbleGrid[nextIndex.y][nextIndex.x];
      List<Vector2Int> neighbours = GetNeighbourIndexes(nextIndex, false);
      foreach (var n in neighbours)
      {
        if (!visited.Contains(n))
          queue.Enqueue(n);

        if (bubbleGrid[n.y][n.x].connected && !nextBubble.connected)
        {
          nextBubble.connected = true;
          disconnected.Remove(nextBubble);
        }
      }

      if (!nextBubble.connected)
        continue;
      
      foreach (var index in neighbours)
      {
        Bubble neighbour = bubbleGrid[index.y][index.x];
        neighbour.connected = true;
        disconnected.Remove(neighbour);
      }
    }

    return disconnected.ToList();
  }

  // Return index of last non-empty row
  private int LastRow()
  {
    for (int y = gridSizeY - 1; y >= 0; y--)
    {
      bool rowEmpty = true;
      foreach (var element in bubbleGrid[y])
      {
        if (element is null)
          continue;
        rowEmpty = false;
      }

      if (!rowEmpty)
        return y;
    }

    return -1;
  }
  
  private void MoveGrid(ref List<Bubble> shiftBubbles)
  {
    oddRow = !oddRow;
    
    // Find first empty row and fill shift bubbles
    int firstEmpty = -1;
    bool rowEmpty;
    for (int y = 0; y < gridSizeY; y++)
    {
      rowEmpty = true;
      foreach (var element in bubbleGrid[y])
      {
        if (element is null)
          continue;
        rowEmpty = false;
        shiftBubbles.Add(element);
      }

      if (rowEmpty)
      {
        firstEmpty = y;
        break;
      }
    }
     
#if UNITY_EDITOR
    Assert.IsTrue(firstEmpty != -1);
    Debug.Log("Last row: " + firstEmpty);
#endif
    
    // Copy all rows one index up
    for (int y = firstEmpty - 1; y >= 0; y--)
    {
      for (int x = 0; x < gridSizeX; x++)
      {
        bubbleGrid[y + 1][x] = bubbleGrid[y][x];
        if (bubbleGrid[y][x] is Bubble)
          bubbleGrid[y + 1][x].gridIndex.y = y + 1;
      }
    }

    // Spawn new row of Bubbles
    for (int x = 0; x < gridSizeX; x++)
    {
      Bubble bubble = bubblePool.Get(GridToWorld(x, 0), BubbleType.StaticBubble);
      bubble.gridIndex.Set(x, 0);
      bubble.connected = true;
      BubbleData bd = GetRandomBubbleData();
      bubble.value = bd.value;
      bubble.meshRenderer.material = bd.material;
      bubbleGrid[0][x] = bubble;
    }
  }
  
#if UNITY_EDITOR || UNITY_STANDALONE
  public List<Bubble> GetDebugExplosionBubbles(Vector3 explosionCenter, float explosionRadius)
  {
    List<Bubble> explosionBubbles = new List<Bubble>();
    for (int y = 0; y < gridSizeY; y++)
    {
      for (int x = 0; x < gridSizeX; x++)
      {
        if (bubbleGrid[y][x] is null)
          continue;

        float distanceToExplosion = Vector2.Distance(explosionCenter, bubbleGrid[y][x].transform.position);
        if (distanceToExplosion >= explosionRadius)
          continue;
        
        explosionBubbles.Add(bubbleGrid[y][x]);
      }
    }

    return explosionBubbles;
  }
#endif
}
