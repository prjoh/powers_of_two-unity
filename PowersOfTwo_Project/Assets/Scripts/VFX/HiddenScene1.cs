using System.Collections;
using UnityEngine;

public class HiddenScene1 : MonoBehaviour
{
  public GameObject pointLight;
  public Transform[] bubbleSpawns;
  public ParticleSystem smoke;
  public Transform humanTransform;
  public Transform humanGoal;
  public Animator humanAnimator;

  private bool activated;
  
  private bool humanMoving;
  private const float humanDelay = 3.0f;
  private float activationTime;
  
  void Start()
  {
    pointLight.SetActive(false);
    smoke.Stop();
    activated = false;
    humanMoving = false;
  }

  // Update is called once per frame
  void Update()
  {
    if (!humanMoving && activated)
    {
      float current = Time.time;

      if (current - activationTime >= humanDelay)
      {
        humanAnimator.Play("Run");
        humanMoving = true;
        StartCoroutine(MoveHuman());
      }
    }

    // TODO: Implement Bubble spawn and movement
  }

  public void Activate()
  {
    pointLight.SetActive(true);
    activated = true;
    smoke.Play();
    activationTime = Time.time;
  }

  private IEnumerator MoveHuman()
  {
    Vector3 _from = humanTransform.position;
    float _duration = 2.0f;
    float _elapsed = 0.0f;
    
    while (true)
    {
      _elapsed += Time.deltaTime;
      humanTransform.position = Vector3.Lerp(_from, humanGoal.position, Easing.Linear(_elapsed / _duration));

      if (_elapsed < _duration)
        yield return null;
      else
      {
        humanTransform.position = humanGoal.position;
        humanAnimator.enabled = false;
        yield break;
      }
    }
  }
}
