using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhoneCamera : MonoBehaviour
{
  private bool camAvailable;
  private WebCamTexture frontCamera;
  private WebCamTexture backCamera;
  private Texture defaultBackground;

  public RawImage background;
  public AspectRatioFitter fit;

  void Start()
  {
    defaultBackground = background.texture;
    WebCamDevice[] devices = WebCamTexture.devices;

    if (devices.Length == 0)
    {
      Debug.Log("No camera detected.");
      camAvailable = false;
      return;
    }

    foreach (var cam in devices)
    {
      if (cam.isFrontFacing)
        frontCamera = new WebCamTexture(cam.name, Screen.width, Screen.height);
      
      if (!cam.isFrontFacing)
        backCamera = new WebCamTexture(cam.name, Screen.width, Screen.height);
    }

    if (backCamera is null && frontCamera is null)
    {
      Debug.Log("Unable to find front- or back-facing camera.");
      return;
    }
    
    frontCamera.Play();
    background.material.SetTexture("_CRTTexture", frontCamera);

    camAvailable = true;
  }

  void Update()
  {
    if (!camAvailable)
      return;

    float ratio = (float) frontCamera.width / (float) frontCamera.height;
    fit.aspectRatio = ratio;

    float scaleY = frontCamera.videoVerticallyMirrored ? -1f : 1f;
    background.rectTransform.localScale = new Vector3(1.0f, scaleY, 1.0f);

    int orient = -frontCamera.videoRotationAngle;
    background.rectTransform.localEulerAngles = new Vector3(0, 0, orient);
  }
}
