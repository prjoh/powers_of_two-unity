using UnityEngine;
using Random = UnityEngine.Random;

public class PointLightFlicker : MonoBehaviour
{
  public Light pointLight;

  private float flickerInterval;
  private float lastFlickerState;
  private float flickerDuration;
  private const float switchFrequency = 0.1f;
  private float lastSwitch;
  private bool flicker;

  private void OnEnable()
  {
    flickerInterval = 3.0f;
    lastFlickerState = Time.time;
    flickerDuration = 2.0f;
    lastSwitch = 0.0f;
    flicker = false;
  }

  // Update is called once per frame
  void Update()
  {
    float current = Time.time;

    if (!flicker && current - lastFlickerState >= flickerInterval)
    {
      flicker = true;
      lastFlickerState = current;
    }

    if (flicker && current - lastFlickerState >= flickerDuration)
    {
      flicker = false;
      lastFlickerState = current;
      pointLight.intensity = 6.0f;
      flickerInterval = Random.Range(3.0f, 5.0f);
      flickerDuration = Random.Range(1.5f, 2.5f);
    }
    else if (flicker && current - lastSwitch >= switchFrequency)
    {
      pointLight.intensity = Random.Range(0.0f, 6.0f);
      lastSwitch = current;
    }
  }
}
