﻿using UnityEngine;

[CreateAssetMenu(fileName = "BubbleData", menuName = "GameData/BubbleData")]
public class BubbleData : ScriptableObject
{
  public int value;
  public float weight;
  public Material material;
}