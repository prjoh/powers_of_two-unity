﻿using System.Collections.Generic;
using UnityEngine;


abstract public class ObjectPool
{
  private GameObject prefab;
  private Stack<GameObject> freeInstances = new Stack<GameObject>();

  public ObjectPool(GameObject _prefab, int _size)
  {
    prefab = _prefab;
    freeInstances = new Stack<GameObject>(_size);

    for (int i = 0; i < _size; ++i)
    {
      GameObject obj = Object.Instantiate(prefab);
      obj.SetActive(false);
      freeInstances.Push(obj);
    }
  }

  public GameObject GetObject()
  {
    return GetObject(Vector3.zero, Quaternion.identity);
  }

  public GameObject GetObject(Vector3 pos, Quaternion quat)
  {
    GameObject ret = freeInstances.Count > 0 ? freeInstances.Pop() : Object.Instantiate(prefab);

    ret.transform.position = pos;
    ret.transform.rotation = quat;
    ret.SetActive(true);

    return ret;
  }

  public void FreeObject(GameObject obj)
  {
    obj.SetActive(false);
    obj.transform.SetParent(null);
    freeInstances.Push(obj);
  }
}