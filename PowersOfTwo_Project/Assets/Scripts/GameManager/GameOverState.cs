﻿using UnityEngine;


public class GameOverState : AState
{
  public GameObject menuUI;

  public override void Enter(AState from)
  {
    menuUI.SetActive(true);
  }

  public override void Exit(AState to)
  {
    menuUI.SetActive(false);
  }

  public override void Tick()
  {

  }

  public override string GetName()
  {
    return "GameOver";
  }
}