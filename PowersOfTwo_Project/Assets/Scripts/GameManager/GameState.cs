﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;


public class GameState : AState
{
  public Camera mainCamera;
  public GameObject gameUI;
  public GridManager gridManager;
  public Transform shooterSpawn;
  public TextMeshProUGUI scoreText;
  public GameObject projectedInputPoint;
  public LayerMask raycastMask;
  public GhostBubble ghostBubble;
  public LineRenderer pathRenderer;
  
  public HiddenScene1 hidden1;

  public int startSize;

  public LineRenderer debugLineRenderer;
  
  private int score;
  private int scoreString;
  private float lastScoreUpdate;
  private const float scoreUpdateFrequency = 0.01f;
#if UNITY_EDITOR || UNITY_STANDALONE
  private bool mouseDown;
#endif

  // Input projection
  private const float gridDistance = 3.5f;
  private float gridToCameraDistance;
  private readonly Vector2 topLeftPlane = new Vector2(-2.95625f, 7.4f);
  private readonly Vector2 bottomRightPlane = new Vector2(2.95625f, -3.85f);
  // Input raycast
  private readonly Vector3 raycastFrom = new Vector3(0.0f, -4.6f, gridDistance);  // In world coordinates
  private List<Vector3> hitPoints = new List<Vector3>();
  private Bubble shooterBubble;
  // State flag
  private bool _bubblesMoving;

  private void Awake()
  {
    gridToCameraDistance = gridDistance - mainCamera.transform.position.z;
  }

  public override void Enter(AState from)
  {
    gameUI.SetActive(true);
    gridManager.CreateGrid(startSize);
    //gridManager.CreateGridFromFile();
    shooterBubble = gridManager.SpawnShooter(shooterSpawn.position);
    score = 0;
    scoreString = 0;
    lastScoreUpdate = Time.time;
    mouseDown = false;
    _bubblesMoving = false;
    // Reset all input that might be leftover from menu state
    Input.ResetInputAxes();
    
    // TODO: Debug
    hidden1.Activate();
  }

  public override void Exit(AState to)
  {
    gameUI.SetActive(false);
    gridManager.DestroyGrid();
    gridManager.DestroyBubble(shooterBubble);
  }

  public override void Tick()
  {
    if (!_bubblesMoving)
      HandleInput();
    UpdateUI();
  }

  public override string GetName()
  {
    return "Game";
  }

  private void HandleInput()
  {
#if UNITY_EDITOR || UNITY_STANDALONE
    if (Input.GetMouseButtonUp(0) && mouseDown)
    {
      mouseDown = false;
      // Check if input is on UI
      if (EventSystem.current.IsPointerOverGameObject())
        return;
      HandleTouchUp(Input.mousePosition);
    }
    else if (Input.GetMouseButtonDown(0) || mouseDown)
    {
      // Check if input is on UI
      if (EventSystem.current.IsPointerOverGameObject())
      {
        HidePath();
        return;
      }
      mouseDown = true;
      HandleTouchDown(Input.mousePosition);
    }
    else if (Input.GetMouseButtonDown(1))
    {
      PerformTest(Input.mousePosition);
    }
#endif

    // Handle screen touches.
    if (Input.touchCount > 0)
    {
      Touch touch = Input.GetTouch(0);

      // TODO: This needs to be tested on a real mobile platform (e.g. down in game, drag to menu, up, then down in game again)
      if (EventSystem.current.IsPointerOverGameObject(touch.fingerId))
      {
        HidePath();
        return;
      }
      if (touch.phase == TouchPhase.Began || touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
        HandleTouchDown(touch.position);
      else if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled)
        HandleTouchUp(touch.position);
    }
  }

  private Vector3 ScreenToProjectionPlane(Vector2 screenSpacePixelPos)
  {
    Vector3 worldPoint = mainCamera.ScreenToWorldPoint(new Vector3(screenSpacePixelPos.x, screenSpacePixelPos.y, gridToCameraDistance));
    worldPoint.Set(Mathf.Clamp(worldPoint.x, topLeftPlane.x, bottomRightPlane.x), 
                   Mathf.Clamp(worldPoint.y, bottomRightPlane.y, topLeftPlane.y),
                   worldPoint.z);
    return worldPoint;
  }

  private void HandleTouchDown(Vector2 screenSpacePixelPos)
  {
#if UNITY_EDITOR
    Debug.Log("Handling touch down at " + screenSpacePixelPos);
#endif
    Vector3 inputWorldPoint = ScreenToProjectionPlane(screenSpacePixelPos);
    
    RaycastHit2D first = new RaycastHit2D();
    first.point = new Vector2(raycastFrom.x, raycastFrom.y);
    first.normal = new Vector2(0.0f, bottomRightPlane.x - topLeftPlane.x).normalized;
    Vector2 direction = (inputWorldPoint - raycastFrom).normalized;

#if UNITY_EDITOR
    projectedInputPoint.transform.position = inputWorldPoint;
    Debug.DrawRay(new Vector3(0.0f, raycastFrom.y, gridDistance), first.normal, Color.magenta);
    Debug.DrawRay(raycastFrom, direction, Color.green);
#endif
    
    hitPoints.Clear();
    hitPoints.Add(shooterSpawn.position);
    GetNextHit(first, direction);

    DrawPath();
  }

  private void GetNextHit(RaycastHit2D previousHit, Vector2 previousOutDirection)
  {
    RaycastHit2D hit = Physics2D.Raycast(previousHit.point + previousOutDirection * 0.01f, previousOutDirection, Mathf.Infinity, raycastMask);

    // If it hits something...
    if (hit.collider) {
      if (hit.collider.CompareTag("RayCollider2D"))
      {
        Vector2 nextOutDirection = Vector2.Reflect(previousOutDirection, hit.normal);
#if UNITY_EDITOR
        Debug.DrawLine(new Vector3(previousHit.point.x, previousHit.point.y, gridDistance), 
          new Vector3(hit.point.x, hit.point.y, gridDistance), 
          Color.red);
        Debug.DrawRay(new Vector3(hit.point.x, hit.point.y, gridDistance), hit.normal, Color.magenta);
        Debug.DrawRay(new Vector3(hit.point.x, hit.point.y, gridDistance), nextOutDirection, Color.green);
#endif
        float hitX = Math.Sign(hit.point.x) * (Math.Abs(hit.point.x) + gridManager.BubbleDiameter * 0.5f);
        hitPoints.Add(new Vector3(hitX, hit.point.y, gridDistance));
        GetNextHit(hit, nextOutDirection);
      }
      else if (hit.collider.CompareTag("BubbleCollider"))
      {
        Vector3 emptyPosition = new Vector3();
        gridManager.GetEmptyNeighbour(hit.collider.gameObject, hit.point, out shooterBubble.gridIndex, out emptyPosition);
        hitPoints.Add(emptyPosition);
        
#if UNITY_EDITOR
        Debug.DrawLine(new Vector3(previousHit.point.x, previousHit.point.y, gridDistance), 
                       new Vector3(hit.point.x, hit.point.y, gridDistance), 
                        Color.red);
#endif
      }
    }
  }

  private void HandleTouchUp(Vector2 screenSpacePixelPos)
  {
    Debug.Log("Handling touch up at " + screenSpacePixelPos);
    // Moves shooter bubble along shooter path, performs merge of bubbles, performs non-connected movement, executes explosions
    // Finally spawns the next shooter bubble and sets the _bubblesMoving flag to false
    StartCoroutine(MoveBubbles());
  }
  
  private void DrawPath()
  {
    // Visual hack for raycast close to wall colliders:
    // Removes second-last point, if it lies behind end point
    if (hitPoints[hitPoints.Count - 1].y <= hitPoints[hitPoints.Count - 2].y)
      hitPoints.RemoveAt(hitPoints.Count - 2);
      
    pathRenderer.positionCount = hitPoints.Count;
    pathRenderer.SetPositions(hitPoints.ToArray());

    ghostBubble.meshRenderer.material.color = shooterBubble.meshRenderer.material.color;
    ghostBubble.gameObj.transform.position = hitPoints[hitPoints.Count - 1];
    ghostBubble.gameObj.SetActive(true);
  }

  private void HidePath()
  {
    pathRenderer.positionCount = 0;
    ghostBubble.gameObj.SetActive(false);
  }

  private void UpdateUI()
  {
    if (score == 0)
      return;
    
    if (Time.time - lastScoreUpdate >= scoreUpdateFrequency)
    {
      score -= 1;
      scoreString += 1;
      scoreText.SetText("Score: " + scoreString);
      lastScoreUpdate = Time.time;
    }
  }

  private void GameOver()
  {
    Debug.Log("Game Over!!!");
    // StopAllCoroutines();
    manager.SwitchState("GameOver");
  }

  private IEnumerator MoveBubbles()
  {
    _bubblesMoving = true;
    // Perform bubble merge
    List<Vector3> mergeLocations = new List<Vector3>();
    List<List<Bubble>> mergeBubbles = new List<List<Bubble>>();
    List<List<Bubble>> disconnectedBubbles = new List<List<Bubble>>();
    List<Bubble> explodingBubbles = new List<Bubble>();
    List<Bubble> shiftBubbles = new List<Bubble>();
    gridManager.MergeBubbles(shooterBubble, ref mergeLocations, ref mergeBubbles, ref disconnectedBubbles, ref explodingBubbles, ref shiftBubbles);
    Debug.Log(mergeLocations.Count + "," + disconnectedBubbles.Count);
#if UNITY_EDITOR
    Assert.IsTrue(mergeLocations.Count == mergeBubbles.Count);
    // The number of disconnected lists is determined by whether the final bubble explodes or not
    // I.e., most of the time, only (mergeLocations.Count == disconnectedBubbles.Count - 1) will evaluate to true
    Assert.IsTrue(mergeLocations.Count == disconnectedBubbles.Count || mergeLocations.Count == disconnectedBubbles.Count - 1);
#endif
    
    // Get shooter bubble path
    Vector3[] shooterPath = new Vector3[pathRenderer.positionCount - 1];
    for (int i = 1; i < pathRenderer.positionCount; i++)
      shooterPath[i-1] = pathRenderer.GetPosition(i);
    HidePath();

    bool shooterMoving = true;
    StartCoroutine(shooterBubble.MoveAlongPath(shooterPath, 20.0f, Easing.Linear, 
        () =>
        {
          shooterMoving = false;
        }
      )
    );
    yield return new WaitUntil(() => !shooterMoving);
    yield return new WaitForSeconds(0.2f); // TODO: Debugging
    shooterBubble.gameObj.layer = LayerMask.NameToLayer("BubbleStatic");

    for (int i = 0; i < mergeLocations.Count; i++)
    {
      // We remove our goal bubble from the list
      Bubble goalBubble = mergeBubbles[i][mergeBubbles[i].Count - 1];
      mergeBubbles[i].RemoveAt(mergeBubbles[i].Count - 1);
      bool[] movingFlags1 = Enumerable.Repeat(true, mergeBubbles[i].Count).ToArray();
      for (int j = 0; j < mergeBubbles[i].Count; j++)
      {
        Bubble bubble = mergeBubbles[i][j];
        movingFlags1[j] = true;
        int _j = j; // Create local copy of j to pass into closure
        StartCoroutine(bubble.MoveTo(mergeLocations[i], 10.0f, Easing.Linear, () => { movingFlags1[_j] = false; }));
      }
      // TODO: Maybe here, we can let our disconnecetd bubbles start falling down
      if (disconnectedBubbles[i].Count > 0)
        RemoveDisconnected(disconnectedBubbles[i]);
      // Wait until all Bubbles of merge i have arrived at their goal position
      yield return new WaitUntil((() => { return movingFlags1.All(x => !x); }));
      // Destroy all Bubbles of merge i
      foreach (var bubble in mergeBubbles[i])
      {
        score += bubble.value;
        gridManager.DestroyBubble(bubble);
      }
      // TODO: DEBUG: actually bubbles will be destroyed upon hitting the ground and NOT HERE
      // foreach (var bubble in disconnectedBubbles[i])
      // {
      //   score += bubble.value;
      //   gridManager.DestroyBubble(bubble);
      // }
      // TODO: DEBUG END
      // Update material according to new value of goal Bubble; we have to recalculate because goalBubble.value is equal to final value
      int goalBubbleValue = mergeBubbles[i][0].value + mergeBubbles[i].Count;
      goalBubble.meshRenderer.material = gridManager.bubbleData[goalBubbleValue - 1].material;
      yield return new WaitForSeconds(0.2f); // TODO: Debugging
    }

    // This is the special case of the final bubble merging into a disconnected region
    // This means, INSIDE the if statement most of the time (disconnectedBubbles[disconnectedBubbles.Count - 1].Count == 0) will evaluate to true
    if (mergeLocations.Count == disconnectedBubbles.Count - 1 && disconnectedBubbles[disconnectedBubbles.Count - 1].Count > 0)
      RemoveDisconnected(disconnectedBubbles[disconnectedBubbles.Count - 1]);

    // TODO: Make bubbles explode
    foreach (var bubble in explodingBubbles)
    {
      score += bubble.value;
      gridManager.DestroyBubble(bubble);
    }
    
    bool[] movingFlags2 = Enumerable.Repeat(true, shiftBubbles.Count).ToArray();
    for (int i = 0; i < shiftBubbles.Count; i++)
    {
      Bubble bubble = shiftBubbles[i];
      Vector3 pos = bubble.gameObj.transform.position + gridManager.BubbleShiftOffset;
      int _i = i; // Create local copy of j to pass into closure
      StartCoroutine(bubble.MoveTo(pos, 2.5f, Easing.InOutQuad, () => { movingFlags2[_i] = false; }));
    }
    yield return new WaitUntil((() => { return movingFlags2.All(x => !x); }));

    shooterBubble = gridManager.SpawnShooter(shooterSpawn.position);
    _bubblesMoving = false;
    
    if (gridManager.GridOverflow)
      GameOver();
  }

  private void RemoveDisconnected(List<Bubble> disconnected)
  {
    Vector3 centroid = new Vector3();
    foreach (var bubble in disconnected)
      centroid += bubble.gameObj.transform.position;
    centroid /= disconnected.Count;
#if UNITY_EDITOR
    DrawCircle(centroid, 2.0f);
#endif
    foreach (var bubble in disconnected)
    {
      score += bubble.value;
      bubble.gameObj.layer = LayerMask.NameToLayer("BubbleFalling");
      bubble.Disconnect((bubble.transform.position - centroid).normalized * 6.0f);
    }
  }
  
#if UNITY_EDITOR || UNITY_STANDALONE
  private void PerformTest(Vector2 screenSpacePixelPos)
  {
    //
    // Explosion Test
    //
    Vector3 worldPoint = mainCamera.ScreenToWorldPoint(new Vector3(screenSpacePixelPos.x, screenSpacePixelPos.y, gridToCameraDistance));
    float radius = 2f;
    DrawCircle(worldPoint, radius);

    Vector3 explosionCenter = new Vector3(worldPoint.x, worldPoint.y, gridDistance);
    List<Bubble> explosionBubbles = gridManager.GetDebugExplosionBubbles(explosionCenter, radius);
    foreach (var bubble in explosionBubbles)
    {
      debugLineRenderer.positionCount += 2;
      debugLineRenderer.SetPosition(debugLineRenderer.positionCount - 2, explosionCenter);
      debugLineRenderer.SetPosition(debugLineRenderer.positionCount - 1, bubble.transform.position);
      bubble.gameObj.layer = LayerMask.NameToLayer("BubbleFalling");
      bubble.Disconnect((bubble.transform.position - explosionCenter).normalized * 6.0f);
    }
  }

  private void DrawCircle(Vector3 posiion, float radius)
  {
    debugLineRenderer.positionCount = 0;
    debugLineRenderer.positionCount = 360;

    for (int i = 0; i < debugLineRenderer.positionCount; i++)
    {
      var rad = Mathf.Deg2Rad * (i * 360f / debugLineRenderer.positionCount);
      debugLineRenderer.SetPosition(i, new Vector3(posiion.x + Mathf.Sin(rad) * radius, posiion.y + Mathf.Cos(rad) * radius, posiion.z));
    }
  }
#endif
}
