﻿using System.Collections.Generic;
using UnityEngine;


public class GameManager : MonoBehaviour
{
  public AState[] states;

  private List<AState> stateStack = new List<AState>();
  private Dictionary<string, AState> stateDict = new Dictionary<string, AState>();


  private void Start()
  {
    stateDict.Clear();

    if (states.Length == 0) return;

    for (int i = 0; i < states.Length; i++)
    {
      states[i].manager = this;
      stateDict.Add(states[i].GetName(), states[i]);
    }

    stateStack.Clear();
    PushState(states[0].GetName());
  }

  private void Update()
  {
    if (stateStack.Count > 0)
    {
      stateStack[stateStack.Count-1].Tick();
    }
  }

  public void SwitchState(string newState)
  {
    AState state;
    if (!stateDict.TryGetValue(newState, out state))
    {
      Debug.LogError("Can't find the state named " + newState + "!");
      return;
    }

#if UNITY_EDITOR
    Debug.Log("Entering state " + newState);
#endif
    
    stateStack[stateStack.Count-1].Exit(state);
    state.Enter(stateStack[stateStack.Count-1]);
    stateStack.RemoveAt(stateStack.Count-1);
    stateStack.Add(state);
  }

  public void PopState()
  {
    if (stateStack.Count < 2)
    {
      Debug.LogError("Can't pop states, only one state in stack!");
      return;
    }
    stateStack[stateStack.Count-1].Exit(stateStack[stateStack.Count-2]);
    stateStack[stateStack.Count-2].Enter(stateStack[stateStack.Count-2]);
    stateStack.RemoveAt(stateStack.Count-1);
  }

  public void PushState(string stateName)
  {
    AState state;
    if (!stateDict.TryGetValue(stateName, out state))
    {
      Debug.LogError("Can't find the state named " + stateName + "!");
      return;
    }

#if UNITY_EDITOR
    Debug.Log("Entering state " + stateName);
#endif

    if (stateStack.Count > 0)
    {
      stateStack[stateStack.Count-1].Exit(state);
      state.Enter(stateStack[stateStack.Count-1]);
    }
    else
    {
      state.Enter(null);
    }
    stateStack.Add(state);
  }
}

public abstract class AState : MonoBehaviour
{
  [HideInInspector]
  public GameManager manager;

  public abstract void Enter(AState from);
  public abstract void Exit(AState to);
  public abstract void Tick();
  public abstract string GetName();
}

